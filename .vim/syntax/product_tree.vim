" Vim syntax file
" Language: Amazon Monetization Product Tree
" Maintainer: Brennan Ruthardt rutharb@

if exists("b:current_syntax")
  finish
endif

" Keywords
syn keyword syntaxElementKeyword containsAny Basis RoundBeforeAugment TaxExclusive TaxInclusive Property Incremental RateCard Refund Charge true false in
syn keyword region EU DE US IN FR IT GB UK IT ES JP NA CA MX BR CN
syn keyword currency GBP EUR INR AED CNY JPY AUD BRL MXN CAD USD 

" Matches
syn match decorator /^\s*@.*$/
syn match oper "\v\=\="
syn match oper "\v\="
syn match oper "\v\!\="
syn match oper "\v\=\>"
syn match oper "\v\&\&"
syn match oper "\v\*"
syn match oper "\v\+"
syn match comment "\v\/\/.*"

" Regions
syn region string start=/\v"/ skip=/\v\\./ end=/\v"/

" Integer with - + or nothing in front
syn match number '\d\+'
syn match number '[-+]\d\+'

" " Floating point number with decimal no E or e 
syn match number '[-+]\d\+\.\d*'

" Floating point like number with E and no decimal point (+,-)
syn match number '[-+]\=\d[[:digit:]]*[eE][\-+]\=\d\+'
syn match number '\d[[:digit:]]*[eE][\-+]\=\d\+'

" Floating point like number with E and decimal point (+,-)
syn match number '[-+]\=\d[[:digit:]]*\.\d*[eE][\-+]\=\d\+'
syn match number '\d[[:digit:]]*\.\d*[eE][\-+]\=\d\+'

let b:current_syntax = 'product_tree'

"hi def link decorator             Constant
highlight   decorator             ctermfg=Yellow
highlight   syntaxElementKeyword  ctermfg=83
highlight   region                ctermfg=DarkCyan
highlight   number                ctermfg=79
highlight   comment               ctermfg=DarkGray
highlight   oper                  ctermfg=Red
highlight   string                ctermfg=202
highlight   currency              ctermfg=140

"0	    0	    Black
"	    1	    4	    DarkBlue
"	    2	    2	    DarkGreen
"	    3	    6	    DarkCyan
"	    4	    1	    DarkRed
"	    5	    5	    DarkMagenta
"	    6	    3	    Brown, DarkYellow
"	    7	    7	    LightGray, LightGrey, Gray, Grey
"	    8	    0*	    DarkGray, DarkGrey
"	    9	    4*	    Blue, LightBlue
"	    10	    2*	    Green, LightGreen
"	    11	    6*	    Cyan, LightCyan
"	    12	    1*	    Red, LightRed
"	    13	    5*	    Magenta, LightMagenta
"	    14	    3*	    Yellow, LightYellow
"	    15	    7*	    White
"

