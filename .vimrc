" Settings
set number
set hidden

"Remaps
silent! nmap <C-p> :NERDTreeToggle<CR>

" Plugins
let g:airline_powerlinefonts = 1

" Commands
command Bd bp\|bd \#

" Pathogen
execute pathogen#infect()
call pathogen#helptags()
